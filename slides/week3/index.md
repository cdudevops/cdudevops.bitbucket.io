<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
# <span>Skill Up:</span> <span>Code</span>



<!-- .slide: data-background-image="../images/bg-mouse.jpg" data-audio-src="audio/devtools-1.ogg" -->
### Chrome Developer tools - repeated

Note: 
We discussed Chrome Developer Tools last week. What are these and how do they help us develop


<!-- .slide: data-background-image="../images/bg-mouse.jpg" data-audio-src="audio/dom-inspector-intro.ogg" -->
### Chrome Developer tools: DOM Inspector
![Chrome dev tools elements](images/chrome-dev-elements.png)

Note:
Click the elements tab and look at the contents. It should look just like HTML you wrote for the exercise. This is not the same as the source code though as it represents the elements currently on the page. If you used javascript to add or remove an element it would appear here. This is a representation of the Document Object Model or DOM. You can just think of the DOM as how the browser keeps track of what’s on the page.



<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Programming with Java
This is for those who are ahead, who want to get started on their interactions for the website. While we will be showing you how to do basic visual responses in CSS3 (eg hover:) we want you to do more useful interactions like validation for the form due in assessment 2


<!-- .slide: data-background-image="../images/bg-mouse.jpg"  -->
### Example Form
![Rego Form](images/Rego_Form.png)

Note:
We will look at a registration form, but not <strong>you cannot do this for your assessment</strong>, we need a form that is relevant to your domain. We use a Registration form only as a demo.
See [Example Rego Form](https://codepen.io/juff03/pen/OXaXRG)
We will look at how that works


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Registration form
* What part of the program is in the html code
* What is in the css 
* How are they linked? Note: not shown in CodePen
* How is the javascript linked? Again not shown
* What does the javascript do?


<!-- .slide: data-background-image="../images/bg-mouse.jpg"  -->
### Example of form Submission
We now look at the code on a website (not codepen)
See [Hairstyle example](https://cdudevops.bitbucket.io/examples/hairstyle/)
There are two forms here to show you what can or cannot be done on bitbucket.
Right click and select Inspect to see the developer tools


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Chrome Developer tools: Hairstyle window
![Hairstyle Developer tools](images/Hairstyle_Developer_Tools.png)


<!-- .slide: data-background-image="../images/bg-mouse.jpg"  -->
### First the short form top left
![Submit calls OpenPage function](images/OpenPage.png)
function openPage() {

      var pageID = document.getElementById("fname").value;
      //console.log("https://cdudevops.bitbucket.io/examples/hairstyle/html/"+pageID + ".html");
      location.replace("https://cdudevops.bitbucket.io/examples/hairstyle/html/"+pageID+".html");
}
<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Activity: Next the form under the right most image
![Submit calls action_form.html](images/action_form.png)
