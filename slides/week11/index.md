<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
# Lets Code



<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
## Oral preparation
* In your teams grab a whiteboard
* Can you draw page with the HTML, CSS and JS from code https://codepen.io/catkutay/pen/ZErXdxj



<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
## Device testing
You don't always have multiple devices on which to test your site but there are some ways we can do the next best thing.


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Inside of Chrome

You can also simulate devices right inside of Chrome, read through the Google Developers article on Chrome Device Mode

https://developers.google.com/web/tools/chrome-devtools/device-mode/ 


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Mobile testing websites

Upload your page/s to your Git Repository pages first and then use the URL to the page when testing.

* [Mobile Test](http://mobiletest.me/)
* [Google's Mobile-Friendly Test](https://search.google.com/test/mobile-friendly)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Remote debugging on real devices

* [Android](https://developer.chrome.com/docs/devtools/remote-debugging/)
* [iPhone](https://moduscreate.com/blog/enable-remote-web-inspector-in-ios-6/)



<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
## Continuous learning
![Cactus reading a book](https://www.maxpixel.net/static/photo/1x/Cactus-Pot-Book-Reading-Study-Education-Eyes-1059633.jpg)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### So much change
* Web development is constantly changing
* New technologies every year
* New frameworks every year
* New libraries every month


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### I don't use what I started with

Or do I?


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Change is good
* It can be daunting to keep on top of the industry
* But it is also really cool
	* Always more to learn
	* Interesting and exciting
	* Lots of fun, passionate community discussions


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### You need to teach yourself
* New languages, frameworks and tools appear all the time
* Most developers teach themselves

[SO Developer Survey](https://insights.stackoverflow.com/survey/2018/#developer-profile-other-types-of-education)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Great ways to learn


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Engage with the community
* Connect with community groups
* Join online discussions
* Participate in hackathons


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Follow blogs and podcasts
* Keeps you up to date with new technologies
* Lots of shiny new stuff to keep you excited about development


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Create your own project
* Working through a project is a great way to build your skills
	* You get to choose what technology you use
	* No business constraints
* There are two types of personal projects
	* Those you finish
	* Those you learn from and throw away
* You will have 100s of unfinished projects but you will use them to learn new skills which later apply to your work


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Answer questions on Stack Overflow
* Solving problems is the best way to learn
* Look at the questions on stack overflow for something you think you can answer
* Research and answer it. Try to make your answer as strong as possible
* Read the documentation to make sure you're correct
* Write code examples


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Contribute to open source projects
* Working with other peoples code is a great way to learn to write better
* If a library on github doesn't quite do what you want - fork it and modify it so it does


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Where to learn from


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Online courses
* [freecodecamp](https://www.freecodecamp.org/)
* [Codecademy](https://www.codecademy.com/)
* [Khan Academy](https://www.khanacademy.org/computing/computer-programming)
* [Sitepoint](https://www.sitepoint.com/)
* [Udemy](https://www.udemy.com)
* And heaps more


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Blogs and news sources
* [Smashing Magazine](https://www.smashingmagazine.com/)
* [CSS Tricks](https://css-tricks.com/)
* [David Walsh](http://davidwalsh.name/)
* [Mozilla hacks](https://hacks.mozilla.org/)
* [Google ](https://www.youtube.com/user/ChromeDevelopers)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Podcasts
* [Syntax](https://syntax.fm/)
* [Front end happyhour](http://frontendhappyhour.com/)
* [Code pen radio](https://blog.codepen.io/radio/)
* [Big web show](http://5by5.tv/bigwebshow/)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Fun stuff
* [NT Game Designers](https://discord.gg/P5RaTvsE)
* [The daily wtf](https://thedailywtf.com/)
* [Commit strip](http://www.commitstrip.com/en/)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
#### Final resources
* [The front end dev handbook 2018](https://frontendmasters.com/books/front-end-handbook/2018/)
* [DevDocs](http://devdocs.io/)
* [Can I use](https://caniuse.com/)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### The good news
Not many developers are unemployed and looking for work

[SO Developer Survey](https://insights.stackoverflow.com/survey/2018/#employment)


<!-- .slide: data-background-image="../images/bg-mouse.jpg" -->
### Don't worry, just make cool stuff
* [Just build websites](http://justbuildwebsites.com/)
