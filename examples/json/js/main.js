
// importing the fs module
//Seee full sccript https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

//Reading

var data;

//call back Function acting on jsonResponse 
function display(dataIn) {
    console.log("Wait for return")
    data = dataIn;
    console.log(data);
    console.log(data.firstName + " " + data.address.streetAddress);
    var element = document.getElementById('file-content');
    element.innerHTML = data.firstName + " " + data.address.streetAddress;

}
function readFile(file) {
    //fetch data and when returns, send to display function
    fetch(file)
        .then(response => response.json())
        .then(jsonResponse => display(jsonResponse));

}
//read data
readFile("https://cdudevops.bitbucket.io/examples/json/text.json");
//


//Writing to file and download! Javascript cannot access local file system to change files


function saveTextToFile() {
    const saveText = '{"tmp":this_one}';

    // file setting
    const text = saveText;
    const name = "sample.json";
    const type = "text/plain";

    // create file
    //in Node.js
    // const fs = require('fs');

    // fs.appendFile(name, saveText, (err) => {
    //     if (err) throw err;
    //     console.log('Text appended to file');
    // });
}
function saveJsonObjToFile() {
    const saveObj = { "a": 3 }; // tmp

    // file setting
    const text = JSON.stringify(saveObj);
    const name = "sample.json";
    const type = "text/plain";

    // create file
    const a = document.createElement("a");
    const file = new Blob([text], { type: type });
    a.href = URL.createObjectURL(file);
    a.download = name;
    document.body.appendChild(a);
    a.click();
    a.remove();
}

saveTextToFile();

saveJsonObjToFile();

