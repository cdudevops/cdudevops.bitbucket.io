var basket = [];
var output
// private variables
function setup() {
    addItem({
        item: "bread",
        price: 0.5
    });
    addItem({
        item: "butter",
        price: 0.3
    });
    
}
function readFile() {
    output = document.getElementById("ConsoleOutput");
    // basketModule returns an object with a public API we can use to add items and count items
    addItem({
        item: "bread",
        price: 0.5
    });
    addItem({
        item: "butter",
        price: 0.3
    });

    // Outputs: 2
    output.innerHTML += "<p>Items:" + getItemCount();
    // Outputs: 0.8
    output.innerHTML += "</p><p>Total:" + getTotal().toString();

    // public API
    console.log(basket.toString());
}
function doSomething() {
    //eg add sale refund
    for (i = 0; i < basket.length; i++) {
        basket[i] = basket[i] * .8
        output.innerHTML = basket[i]
    }
    // Return an object exposed to the public
}

function addItem(values) {
    basket.push(values);
};
// Get the count of items in the basket
function getItemCount() {
    return basket.length;
};
// Public alias to access a private function directly

// Get the total value of items in the basket
function getTotal() {
    var q = this.getItemCount(),
        p = 0;
    while (q--) {
        p += basket[q].price;
    }
    return p;
};







const message = "How much to you owe?";


