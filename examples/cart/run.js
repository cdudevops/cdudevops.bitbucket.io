
// basketModule returns an object with a public API we can use to add items and count items
addItem({
    item: "drink",
    price: 0.5
});
addItem({
    item: "eggs",
    price: 0.3
});

// Outputs: 2
console.log(getItemCount());
// Outputs: 0.8
console.log(getTotal());
// However, the following will not work:
// Outputs: undefined
// This is because the basket itself is not exposed as a part of our



// This also won't work as it only exists within the scope of our
// basketModule closure, but not in the returned public object
console.log(basket);
